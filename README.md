### Generate namenode_id and resourcemanager_id variables (identifier variables)
python3 utils/hadoop/inventory/generate_node_identifier.py -i inventory/


### images - prepare image with role (target: ['hadoop', 'zookeeper', 'kerberos'])
ansible-playbook -vv --private-key example.pem -b -u vagrant -i inventory/ images/playbooks/hadoop.yml
ansible-playbook -vv --private-key example.pem -b -u vagrant -i inventory/ images/playbooks/zookeeper.yml


### Configure services (hadoop, zookeeper)
ansible-playbook -vv  --private-key example.pem -b -u vagrant -i inventory/ --extra-vars=@vars/test/configure.yml playbooks/hadoop.yml
ansible-playbook -vv --private-key example.pem -b -u vagrant -i inventory/ playbooks/zookeeper.yml


### Test inventory
ansible-playbook -v --private-key example.pem -b -u vagrant -i inventory/ playbooks/tests/inventory.yml


### Restart datanode service
ansible-playbook -v --private-key example.pem -b -u vagrant -i inventory/ playbooks/utils/restart_datanodes.yml


### Send Vagrant file (infrastructure definition) to server
scp Vagrantfile glozanoa@hw:/home/glozanoa/infrastructure/hadoop


### Format hdfs, bootstrap standby namenodes and activate primary namenode
1. Format primary namenode (hdfs namenode -format -force)
2. Format standby namenode (ON EACH standby NODE) (hdfs namenode -bootstrapStandby -force)
3. Check state of namenodes: hdfs haadmin -getAllServiceState
4. Transition a namenode to Active State: hdfs haadmin -ns mycluster -getServiceState nn01
---
zookeeper
1. Format ZKFC (on each namenode):  hdfs zkfc -formatZK
2. Start ZKFC (on each namenode): hdfs zkfc


### ResourceManager admin commands
# source: https://hadoop.apache.org/docs/current/hadoop-yarn/hadoop-yarn-site/YarnCommands.html#rmadmin
- State of resourcemanager node (active or standby): yarn rmadmin -getServiceState rm1
- 



#### Utilities
- ansible-playbook -vv -e hadoop_home=/var/hadoop -e ssh_public_key=$PWD/example.pem.pub --private-key example.pem -b -u vagrant -i inventory/ playbooks/utils/local/add_admin_user.yml
