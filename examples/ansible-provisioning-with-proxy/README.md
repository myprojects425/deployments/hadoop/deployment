
* Generate operations SSH key
NOTE: Execute it before deploy infrastructure
```sh
ssh-keygen -f operations.pem -m PEM -b 4096 -t rsa -q
```

* Get sensitive `private_key_pem` output
NOTE: sensitive doesn't mean secure!!!!

```sh
cat terraform.tfstate | jq -r ".outputs.private_key_pem.value" > instances.pem
chmod 400 instances.pem
```

* Connect to private instance via bastion
NOTE: update ssh-config.example file (with output of 'terraform output')
```sh
ssh -F ssh-config.example -i instances.pem ubuntu@ip-10-0-1-58.ec2.internal
```

* Excute playbook on private instances via bastion
NOTE: update ssh-config.example file
```sh
ansible-playbook -v --ssh-extra-args "-F ./ssh-config.example" -i inventory.yaml --private-key instances.pem playbooks/example.yml
```

#### References
- [Using an Ansible playbook with an SSH bastion / jump host](https://www.jeffgeerling.com/blog/2022/using-ansible-playbook-ssh-bastion-jump-host)
