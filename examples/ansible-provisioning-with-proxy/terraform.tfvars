aws_region = "us-east-1"
instance_type = "t3.micro"

# bastion variables
operations_key_name   = "operations-virg"
operations_public_key_file = "operations.pem.pub"


# private instance variables
instance_key_name = "private-virg"
number_instances  = 2
