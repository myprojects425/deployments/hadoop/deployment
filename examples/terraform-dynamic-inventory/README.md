* Install requirements
```sh
python3 -m pip install -r requirements.txt
ansible-galaxy collection install -r requirements.yml
```

* Inspect dynamic invetory
```sh
ansible-inventory -i inventory.yaml --list
```

* Execute a simple playbook to test inventory
```sh
ansible-playbook --private-key path/to/key.pem -i inventory.yaml playbooks/example.yml
```
