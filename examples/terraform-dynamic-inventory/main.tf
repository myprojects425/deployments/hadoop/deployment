terraform {
  required_providers {                     #### ansible provider
ansible = {
      version = "~> 1.1.0"
      source  = "ansible/ansible"
    }
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region = var.aws_region
}

data "aws_vpc" "default" {
  default = true
}

data "aws_subnets" "default" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.default.id]
  }

  filter {
    name   = "state"
    values = ["available"]
  }
}

resource "aws_security_group" "allow_all" {
  # source: https://hadoop.apache.org/docs/stable/hadoop-project-dist/hadoop-hdfs/HdfsUserGuide.html#Web_Interface
  name        = "allow_all_sg"
  description = "Allow SSH inbound traffic"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_all_sg"
  }
}


data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

locals {
  foo_instances = 3
  subnet = tolist(data.aws_subnets.default.ids)[0]
}

resource "aws_instance" "foo" {
  count = local.foo_instances
  ami             = data.aws_ami.ubuntu.id
  instance_type   = var.instance_type
  #vpc_security_group_ids = [aws_security_group.allow_all.id]
  security_groups = [aws_security_group.allow_all.id]
  key_name        = var.key_pair
  subnet_id   = local.subnet
  associate_public_ip_address = true


  tags = {
    Name = "foo${count.index}"
  }
}


resource "aws_instance" "bar" {
  ami             = data.aws_ami.ubuntu.id
  instance_type   = var.instance_type
  #vpc_security_group_ids = [aws_security_group.allow_all.id]
  security_groups = [aws_security_group.allow_all.id]
  key_name        = var.key_pair
  subnet_id   = local.subnet
  associate_public_ip_address = true


  tags = {
    Name = "bar"
  }
}


resource "ansible_host" "foo" {          #### ansible host details
  count = local.foo_instances
  name   = "foo-${count.index}"
  groups = ["foo"]
  variables = {
    ansible_user                 = "ubuntu",
    ansible_python_interpreter   = "/usr/bin/python3"
    ansible_host = aws_instance.foo[count.index].public_ip

    myvar = "foo is not unique (instance ${local.foo_instances})"
  }
}


resource "ansible_host" "bar" {          #### ansible host details
  name   = "bar"
  groups = ["bar"]
  variables = {
    ansible_user                 = "ubuntu",
    ansible_python_interpreter   = "/usr/bin/python3"
    ansible_host = aws_instance.bar.public_ip

    myvar = "bar is unique"
  }
}


resource "ansible_group" "mynodes" {
  name     = "mynodes"
  children = ["foo", "bar"]
  variables = {
    hello = "from mynodes group!"
  }
}
