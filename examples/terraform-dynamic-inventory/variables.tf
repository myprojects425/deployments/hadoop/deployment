variable "aws_region" {
  type = string
  description = "AWS region"
}


variable "instance_type" {
  type = string
  default = "t3.micro"
  description = "AWS instance type"
}


variable "key_pair" {
  type = string
  description = "Key Pair"
}
